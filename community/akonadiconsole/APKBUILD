# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=akonadiconsole
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x, ppc64le and riscv64 blocked by akonadi
# ppc64le blocked by calendarsupport
arch="all !armhf !s390x !ppc64le !riscv64"
url="https://kontact.kde.org/"
pkgdesc="Application for debugging Akonadi Resources"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	akonadi-contacts-dev
	akonadi-dev
	akonadi-search-dev
	calendarsupport-dev
	extra-cmake-modules
	kcalendarcore-dev
	kcompletion-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcontacts-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kitemmodels-dev
	kitemviews-dev
	kmime-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdepim-dev
	messagelib-dev
	qt5-qtbase-dev
	samurai
	xapian-bindings
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/pim/akonadiconsole.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadiconsole-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
71f483afb29fed659c4c17472fee05b14527f9c51078dade8ba63f8329f6fdca05bd367b3c45e7528a3cbc98a52d8d2ee28bed9612bb701f13c304c51fde3813  akonadiconsole-23.08.0.tar.xz
"
