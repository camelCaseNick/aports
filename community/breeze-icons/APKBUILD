# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=breeze-icons
pkgver=5.109.0
pkgrel=0
pkgdesc="Breeze icon themes"
arch="noarch !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-3.0-or-later"
makedepends="
	extra-cmake-modules
	py3-lxml
	python3
	qt5-qtbase-dev
	samurai
	"
checkdepends="bash"
subpackages="$pkgname-dev"
_repo_url="https://invent.kde.org/frameworks/breeze-icons.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/breeze-icons-$pkgver.tar.xz"

# Several KDE applications use icons not yet present in most themes
# We want to keep the possibility for users to not use the KDE provided
# breeze-icons theme however, as hopefully in the future this situation changes
# Thus let any theme that provides these icons provide "kde-icons" so the user
# retains their ability to choose their preferred theme
provides="kde-icons"
provider_priority=100

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBINARY_ICONS_RESOURCE=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure -E '(dupe|symlink)'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e585f27d0c4a69f00d5645e6ce71a030972c3636cef8c3ab7376f2c44c6d237a41c8b0a46b9f1761cdeff67b347301db572efb6b8c9bfe371fd219b2af9738a6  breeze-icons-5.109.0.tar.xz
"
