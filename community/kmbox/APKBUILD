# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kmbox
pkgver=23.08.0
pkgrel=0
pkgdesc="Library for accessing mail storages in MBox format"
# armhf blocked by extra-cmake-modules
# s390x blocked by kmime
arch="all !armhf !s390x"
url="https://kontact.kde.org/"
license="LGPL-2.0-or-later"
depends_dev="kmime-dev"
makedepends="$depends_dev extra-cmake-modules samurai"
_repo_url="https://invent.kde.org/pim/kmbox.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmbox-$pkgver.tar.xz"
subpackages="$pkgname-dev"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1c73930cac64a0e91e521fa3c5305df68c81d78eef3ddf441eb6e57d204ef1c23bbcad2fbdca9d64c157fd454308ceb915c279adc69929f3828b25029a25bebd  kmbox-23.08.0.tar.xz
"
