# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kamoso
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> purpose
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kde.org/applications/multimedia/org.kde.kamoso"
pkgdesc="An application to take pictures and videos out of your webcam"
license="GPL-2.0-or-later AND LGPL-2.1-only"
depends="
	gst-plugins-bad
	gst-plugins-good
	kirigami2
	qt5-qtquickcontrols2
	"
makedepends="
	extra-cmake-modules
	glib-dev
	gobject-introspection-dev
	gst-plugins-base-dev
	gstreamer-dev
	kconfig-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	knotifications-dev
	purpose-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/multimedia/kamoso.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kamoso-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # Broken

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
fa2f8271103945e3d612ad2dc4de968d685e3a1057414a5bc9a5b6eb65203acf62af6ab8567549edf38dee13ced6a152ab9302078ed036de592bed64e189e142  kamoso-23.08.0.tar.xz
"
