# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kdialog
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/"
pkgdesc="A utility for displaying dialog boxes from shell scripts"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kdbusaddons-dev
	kguiaddons-dev
	kiconthemes-dev
	kio-dev
	knotifications-dev
	ktextwidgets-dev
	kwindowsystem-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/utilities/kdialog.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdialog-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
49ba259756814ad9493693bed5e6482e18ae2d7d61a32bba53412a53468b722fa9896df3cdb733e306f268d921e4de60dec8622875f4a3947b2f6d7160fbdfbc  kdialog-23.08.0.tar.xz
"
