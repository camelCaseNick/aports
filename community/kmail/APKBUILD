# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kmail
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/components/kmail/"
pkgdesc="Email client, supporting POP3 and IMAP mailboxes"
license="GPL-2.0-or-later"
# TODO: Replace gnupg with specific gnupg subpackages that kmail really needs.
depends="kdepim-runtime kmail-account-wizard gnupg"
makedepends="
	akonadi-contacts-dev
	akonadi-dev
	akonadi-mime-dev
	akonadi-search-dev
	extra-cmake-modules
	gpgme-dev
	kbookmarks-dev
	kcalendarcore-dev
	kcalutils-dev
	kcmutils-dev
	kcodecs-dev
	kconfig-dev
	kconfigwidgets-dev
	kcontacts-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kguiaddons-dev
	ki18n-dev
	kiconthemes-dev
	kidentitymanagement-dev
	kio-dev
	kitemviews-dev
	kjobwidgets-dev
	kldap-dev
	kmailtransport
	kmime-dev
	knotifications-dev
	knotifyconfig-dev
	kontactinterface-dev
	kparts-dev
	kpimtextedit-dev
	kservice-dev
	ktextwidgets-dev
	ktnef-dev
	kuserfeedback-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libgravatar-dev
	libkdepim-dev
	libkleo-dev
	libksieve-dev
	mailcommon-dev
	messagelib-dev
	pimcommon-dev
	qt5-qtbase-dev
	qt5-qtwebengine-dev
	samurai
	sonnet-dev
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/pim/kmail.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmail-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# akonadi-sqlite-tagselectdialogtest, akonadi-sqlite-kmcommandstest,
	# akonadi-sqlite-unifiedmailboxmanagertest and akonadi-mysql-unifiedmailboxmanagertestrequire
	# running dbus server
	local skipped_tests="("
	local tests="
		akonadi-sqlite-tagselectdialogtest
		akonadi-sqlite-kmcommandstest
		akonadi-sqlite-unifiedmailboxmanagertest
		akonadi-mysql-unifiedmailboxmanagertest"
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)"
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
0846f3e2f84c3d8efd9602a582188fa5b24eeac8843c033bf240748e3c53b62a08822af249c7d81f1e32fda88cdb158a57f27a99d88f930ca8d59c8b21285b07  kmail-23.08.0.tar.xz
"
