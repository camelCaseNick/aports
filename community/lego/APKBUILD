# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Carlo Landmeter <clandmeter@alpinelinux.org>
pkgname=lego
pkgver=4.14.0
pkgrel=1
pkgdesc="Let's Encrypt client and ACME library written in Go"
url="https://github.com/go-acme/lego"
license="MIT"
arch="all !s390x" # tests fail due to network timeouts
options="net setcap chmod-clean" # tests need network access: https://github.com/go-acme/lego/issues/560
depends="ca-certificates"
makedepends="go libcap-utils"
checkdepends="tzdata"
source="https://github.com/go-acme/lego/archive/v$pkgver/lego-$pkgver.tar.gz"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

case "$CARCH" in
	aarch64)
		# TestChallengeWithProxy/matching_X-Forwarded-Host_(multiple_fields)
		options="$options !check"
		;;
	ppc64le)
		# timeout on doing stuff with a server halfway across the world
		options="$options !check"
		;;
esac

build() {
	go build -v -ldflags "-X main.version=$pkgver" -o ./bin/lego ./cmd/lego
}

check() {
	go test ./...
}

package() {
	install -Dm755 ./bin/lego "$pkgdir"/usr/bin/lego
	setcap cap_net_bind_service=+ep "$pkgdir"/usr/bin/lego
}

sha512sums="
2b8daf5732eb5a97ffaa428df7ce0f262c812b07625a9f7c61d1e019fe45f2d4e0c5de15416fce6897fbbfa9bd412fc63f125116af967b7512ddd2d6fd17d712  lego-4.14.0.tar.gz
"
