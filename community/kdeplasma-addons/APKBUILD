# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=kdeplasma-addons
pkgver=5.27.7
pkgrel=1
pkgdesc="All kind of addons to improve your Plasma experience"
# armhf blocked by qt5-qtdeclarative
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kde.org/plasma-desktop/"
license="LGPL-2.0-only AND GPL-2.0-or-later"
depends="purpose"
depends_dev="
	icu-dev
	karchive-dev
	kcmutils-dev
	kconfig-dev
	kcoreaddons-dev
	kdeclarative-dev
	kholidays-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knewstuff-dev
	knotifications-dev
	kross-dev
	krunner-dev
	kservice-dev
	kunitconversion-dev
	kwindowsystem-dev
	networkmanager-qt-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtwebengine-dev
	sonnet-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/kdeplasma-addons.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/kdeplasma-addons-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	# converterrunnertest is broken
	# datetimerunnertest requires running Wayland session
	xvfb-run ctest --test-dir build --output-on-failure -E "(converterrunner|datetimerunner)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
87cba2e1ebe6c8d7afe518aa2b4729696eb59e1cfd416d826f8249d56c184fd015312377bee4afc046df98157b991b4d258eff48101bd2be26964d07ececfb4c  kdeplasma-addons-5.27.7.tar.xz
"
