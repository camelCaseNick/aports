# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kmediaplayer
pkgver=5.109.0
pkgrel=0
pkgdesc="Media player framework for KDE 5"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="X11 AND LGPL-2.1-or-later"
depends_dev="
	kparts-dev
	kxmlgui-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/frameworks/kmediaplayer.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kmediaplayer-$pkgver.tar.xz"
subpackages="$pkgname-dev"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	# viewtest requires X11 to be running
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1176fe15a6b38553c81321229fbb796d31de34d930e73c0e34891d8dfb0597f214099e0ad0c48d242e19ee20519e01760a4078dcfee76ed26631209304d21817  kmediaplayer-5.109.0.tar.xz
"
