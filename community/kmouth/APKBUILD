# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kmouth
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/utilities/org.kde.kmouth"
pkgdesc="Speech Synthesizer Frontend"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt5-qtbase-dev
	qt5-qtspeech-dev
	samurai
	"
_repo_url="https://invent.kde.org/accessibility/kmouth.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmouth-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DKF_IGNORE_PLATFORM_CHECK=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
8ae95f3dfed292b3d9323254860af2c8e22326074e724e3f1e21a6f37b7b2001c72687422afcb0d8f39974ab166fb9d8d2db1bff172b1d31e72a27123ccbc109  kmouth-23.08.0.tar.xz
"
