# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=krfb
pkgver=23.08.0
pkgrel=0
arch="all !armhf"
url="https://kde.org/applications/internet/org.kde.krfb"
pkgdesc="Desktop sharing"
license="GPL-3.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdnssd-dev
	kdoctools-dev
	ki18n-dev
	knotifications-dev
	kpipewire-dev
	kwallet-dev
	kwayland-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libvncserver-dev
	pipewire-dev
	plasma-wayland-protocols
	qt5-qtbase-dev
	qt5-qtx11extras-dev
	samurai
	xcb-util-dev
	xcb-util-image-dev
	"
_repo_url="https://invent.kde.org/network/krfb.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/krfb-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
83d70d8944149b275e945d671620117c243c31dbf0f0fb57a6eb0a074210edafa971bac7c2b96ff8e5a82fc88c6160fc137ce2c46f23ec7135cc7757f27617ed  krfb-23.08.0.tar.xz
"
