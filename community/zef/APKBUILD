# Contributor: Curt Tilmes <Curt.Tilmes@nasa.gov>
# Maintainer: Curt Tilmes <Curt.Tilmes@nasa.gov>
pkgname=zef
pkgver=0.18.3
pkgrel=0
pkgdesc="Raku / Perl6 Module Management"
url="https://github.com/ugexe/zef"
arch="all !ppc64le !s390x !riscv64" # limited by rakudo
options="!archcheck" # Precompiled arch dependent files included (quiet noarch warning)
license="Artistic-2.0"
depends="rakudo git wget curl tar unzip perl-utils"
makedepends="rakudo-dev"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/ugexe/zef/archive/v$pkgver.tar.gz"

check() {
	prove -e 'raku -Ilib'
}

package() {
	RAKUDO_RERESOLVE_DEPENDENCIES=0 /usr/share/rakudo/tools/install-dist.p6 \
		--to="$pkgdir"/usr/share/rakudo/vendor --for=vendor
	rm "$pkgdir"/usr/share/rakudo/vendor/bin/zef-j \
		"$pkgdir"/usr/share/rakudo/vendor/bin/zef-m
	mkdir -p "$pkgdir"/usr/bin
	ln -s /usr/share/rakudo/vendor/bin/zef "$pkgdir"/usr/bin/zef

	install -Dvm644 LICENSE META6.json README.md \
		-t "$pkgdir"/usr/share/doc/"$pkgname"
}

sha512sums="
b7d87508edb305f8a601103d6bf6c9e0173516fbbad3b0349bf9772fea8c018390555b47b87afe6fd3905e88e1e94b3828df6bc8f55980b97ddf290f1ea26f94  zef-0.18.3.tar.gz
"
