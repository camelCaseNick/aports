# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=solid
pkgver=5.109.0
pkgrel=0
pkgdesc="Hardware integration and detection"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="qt5-qtdeclarative-dev"
makedepends="$depends_dev
	bison
	doxygen
	eudev-dev
	extra-cmake-modules
	flex-dev
	qt5-qttools-dev
	samurai
	udisks2-dev
	upower-dev
	"
_repo_url="https://invent.kde.org/frameworks/solid.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/solid-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-libs $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# solidmttest is broken
	ctest --test-dir build --output-on-failure -E "solidmttest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
efb782dee8b15c95c6faaeb190ec5d355fecdf84b33310d177020e5e564e570dde84fe110002e8f79807bb21506ebd1ffbbf8fb91bd7d0022bcc3cfe2f8c0380  solid-5.109.0.tar.xz
"
