# Maintainer: Andy Postnikov <apostnikov@gmail.com>
pkgname=py3-typeguard
_pkgname=typeguard
pkgver=4.0.1
pkgrel=0
pkgdesc="run-time type checker for Python "
url="https://typeguard.readthedocs.io/"
arch="noarch"
license="MIT"
depends="py3-bracex"
makedepends="py3-gpep517 py3-setuptools_scm py3-wheel"
checkdepends="py3-mypy py3-pytest"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir"/$_pkgname-$pkgver

build() {
	SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver \
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	rm -f tests/mypy/test_type_annotations.py # test is broken
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
2e93086309550d6b98f2087a8900797ab9486c1a969da649d1a6cb508f6154b57b6807762e87df43088f80b69bcc11f87c71d83b38ca205d3ad8878a2cd4eb0e  typeguard-4.0.1.tar.gz
"
