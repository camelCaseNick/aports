# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kwordquiz
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://edu.kde.org/kwordquiz"
pkgdesc="Flash Card Trainer"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kcrash-dev
	kdoctools-dev
	kguiaddons-dev
	ki18n-dev
	kiconthemes-dev
	kirigami-addons-dev
	kirigami2-dev
	kitemviews-dev
	knewstuff-dev
	knotifications-dev
	knotifyconfig-dev
	kwindowsystem-dev
	kxmlgui-dev
	libkeduvocdocument-dev
	phonon-dev
	qt5-qtbase-dev
	qt5-qtmultimedia-dev
	samurai
	"
_repo_url="https://invent.kde.org/education/kwordquiz.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kwordquiz-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
84a251ade5b128c10aa02c1a9b43e2e42e16aaa56422bbb3f114ed8f2d574bf7b84489c8f2d95d7cf79ff626689dbc1c27c7768321c79bca4ff44dcae6de5226  kwordquiz-23.08.0.tar.xz
"
