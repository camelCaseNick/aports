# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Carlo Landmeter <clandmeter@alpinelinux.org>
pkgname=ocrmypdf
pkgver=14.4.0
pkgrel=0
pkgdesc="Add OCR text layer to scanned PDF files"
url="https://github.com/ocrmypdf/OCRmyPDF"
# s390x, riscv64: pngquant
# armhf, armv7, x86, ppc64le: tesseract-ocr
arch="noarch !x86 !armhf !armv7 !ppc64le !s390x !riscv64"
license="MIT"
depends="
	ghostscript
	jbig2enc
	leptonica
	pngquant
	py3-deprecation
	py3-img2pdf
	py3-packaging
	py3-pdfminer
	py3-pikepdf
	py3-pillow
	py3-pluggy
	py3-reportlab
	py3-rich
	py3-tqdm
	python3
	qpdf
	tesseract-ocr
	unpaper
	"
makedepends="
	py3-gpep517
	py3-setuptools_scm
	py3-wheel
	"
checkdepends="
	py3-hypothesis
	py3-pytest
	py3-pytest-cov
	py3-pytest-xdist
	tesseract-ocr-data-eng
	tesseract-ocr-data-osd
	"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/o/ocrmypdf/ocrmypdf-$pkgver.tar.gz"

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	PYTHONPATH=src \
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/ocrmypdf*.whl
}

sha512sums="
6430dba740d7dc2de0ec422395ed8294242a63352d82c49840a11a2735dcd011f954a0a72a43bc023e3b3219e1a945c9712ed1c63ab17efc3388faf021f90195  ocrmypdf-14.4.0.tar.gz
"
