# Contributor: Erwan Rouchet <lucidiot@brainshit.fr>
# Maintainer: Erwan Rouchet <lucidiot@brainshit.fr>
pkgname=liferea
pkgver=1.15.2
pkgrel=0
pkgdesc="Desktop news aggregator for online news feeds and weblogs"
arch="all !riscv64" # webkit2gtk, libpeas
url="https://lzone.de/liferea"
license="GPL-2.0-or-later"
depends="py3-gobject3 libpeas-python3"
makedepends="
	glib-dev
	intltool
	gobject-introspection-dev
	libxml2-dev
	libxslt-dev
	sqlite-dev
	gtk+3.0-dev
	pango-dev
	webkit2gtk-4.1-dev
	json-glib-dev
	gsettings-desktop-schemas-dev
	libpeas-dev
	libsoup3-dev
	"
source="https://github.com/lwindolf/liferea/releases/download/v$pkgver/liferea-$pkgver.tar.bz2"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--disable-schemas-compile
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

doc() {
	default_doc
	mkdir -p "$subpkgdir/usr/share/liferea"
	mv "$pkgdir/usr/share/liferea/doc" "$subpkgdir/usr/share/liferea/"
}

sha512sums="
01bad0a64bb0b2c9b9cefcbe4b59e3538145e3572ed7a85034fe7618bf7e2b195ea0307ff5a12b9a5df1afddb544a16ac53eb165ffadb2f38b28b261e9ad62f3  liferea-1.15.2.tar.bz2
"
