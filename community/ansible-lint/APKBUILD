# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=ansible-lint
pkgver=6.18.0
pkgrel=1
pkgdesc="check ansible playbooks"
url="https://github.com/ansible/ansible-lint"
arch="noarch"
options="!check"
license="MIT"
depends="
	ansible-core
	black
	git
	py3-ansible-compat
	py3-filelock
	py3-jinja2
	py3-jsonschema
	py3-packaging
	py3-rich
	py3-ruamel.yaml
	py3-wcmatch
	py3-yaml
	python3
	yamllint
	"
makedepends="
	py3-gpep517
	py3-installer
	py3-setuptools
	py3-setuptools_scm
	py3-wheel
	"
checkdepends="
	py3-flaky
	py3-psutil
	py3-pytest
	py3-pytest-cov
	py3-pytest-xdist
	yamllint
	"
subpackages="$pkgname-pyc"
source="ansible-lint-$pkgver.tar.gz::https://github.com/ansible-community/ansible-lint/archive/refs/tags/v$pkgver/ansible-lint-v$pkgver.tar.gz
	no-version-check.patch
	"
provides="py3-ansible-lint=$pkgver-r$pkgrel" # for backward compatibility
replaces="py3-ansible-lint" # for backward compatibility

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/ansible_lint-$pkgver-py3-none-any.whl
}

sha512sums="
664bff786ac673a1f0c7d369dfabaeb00b54ca6be00ab52e1d2002b21dcf881f31fa78ec49d1428efa1693d47b9880793e15316ec34e69ae396d7cdfd292cb8a  ansible-lint-6.18.0.tar.gz
314fa02e0b30db8a8886824b0cce825ae4ffe227e2c5be434bc96e1c3ab8a6239548574d9ed0869def67b94c684a67abbf594f78aadbc64286fe8187502ba275  no-version-check.patch
"
