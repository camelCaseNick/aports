# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=akonadi-import-wizard
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by kmailtransport -> libkgapi -> qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
pkgdesc="Import data from other mail clients to KMail"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	akonadi-dev>=$pkgver
	extra-cmake-modules
	kauth-dev
	kconfig-dev
	kcontacts-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kidentitymanagement-dev
	kio-dev
	kmailtransport-dev
	kwallet-dev
	libkdepim-dev
	mailcommon-dev
	messagelib-dev
	pimcommon-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/pim/akonadi-import-wizard.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-import-wizard-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
663f20f6f16cffb4aa09cbcc30591e7290ee2cb6a54afd8f342b7360821e3ccea538c23b6c0e6dc355a0f24376a5ad9ae79e66bd2491dba4d1712a8a0ed6f73d  akonadi-import-wizard-23.08.0.tar.xz
"
