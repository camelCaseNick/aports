# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kcolorchooser
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/graphics/org.kde.kcolorchooser"
pkgdesc="A color palette tool, used to mix colors and create custom color palettes"
license="MIT"
makedepends="
	extra-cmake-modules
	ki18n-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/graphics/kcolorchooser.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kcolorchooser-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7e0429c4bd79544e38ca94df04d44e08abe04c3d747d142564f77305e84b3d5d6e607c0f6f0e2d3b61663559288134648bb459cb7bf0032fe2e4bae3b1d9489a  kcolorchooser-23.08.0.tar.xz
"
