# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=libkcddb
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/multimedia/"
pkgdesc="KDE CDDB library"
license="LGPL-2.0-or-later AND GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kcodecs-dev
	kconfig-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	libmusicbrainz-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/multimedia/libkcddb.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkcddb-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="net" # Required for tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_TESTING=ON
	cmake --build build
}

check() {
	# musicbrainztest-severaldiscs fails due to utf16/8 (?)
	# synchttplookuptest hangs
	# asyncmusicbrainztest http request
	# utf8test seems to fail to start dbus-daemon
	xvfb-run -a ctest --test-dir build --output-on-failure \
		-E "(asyncmusicbrainztest|synchttplookuptest|musicbrainztest-severaldiscs|utf8test)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d48cc7c68d152daf32fbbb9787734d4453cce7381efb356d450825892a21515b24770775c2983a66ee26d54aa4cb6bf80b6441d727bd5b7e5b789d2055aefc31  libkcddb-23.08.0.tar.xz
"
