# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kcalc
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/utilities/org.kde.kcalc"
pkgdesc="Scientific Calculator"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	gmp-dev
	kconfig-dev
	kconfigwidgets-dev
	kcrash-dev
	kdoctools-dev
	kguiaddons-dev
	ki18n-dev
	kinit-dev
	knotifications-dev
	kxmlgui-dev
	mpfr-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/utilities/kcalc.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kcalc-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
24342ed2842be846a99dbae0048fd648b3a536aca526878c067bb1de98259b9a14f3eb2164bc5825d7e878a48ca369a0ad48ea09eeae7c2e1ada0c0c65ccffbc  kcalc-23.08.0.tar.xz
"
