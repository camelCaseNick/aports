# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=attica
pkgver=5.109.0
pkgrel=0
pkgdesc="Freedesktop OCS binding for Qt"
url="https://www.kde.org/"
arch="all !armhf" # Blocked by extra-cmake-modules
license="LGPL-2.0-or-later"
makedepends="
	doxygen
	extra-cmake-modules
	qt5-qtbase-dev
	qt5-qttools-dev
	samurai
	"
subpackages="$pkgname-dev $pkgname-doc"
_repo_url="https://invent.kde.org/frameworks/attica.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/attica-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# providertest requires network access
	ctest --test-dir build --output-on-failure -E "providertest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
662f591913d661b6d0f4ab62b1f2f7aa43768ecb914f49f2a98f708df37192b4933f1613f5200a2288d32f7c66da50ce927c31ddb9ab55c0abb6f2fb9611ebaa  attica-5.109.0.tar.xz
"
