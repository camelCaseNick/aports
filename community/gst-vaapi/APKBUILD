# Contributor: Taner Tas <taner76@gmail.com>
# Maintainer: Taner Tas <taner76@gmail.com>
pkgname=gst-vaapi
# NOTE: Upgrade only to even-numbered minor versions (e.g. 1.20.x, 1.22.x)!
#  Odd versions are unstable development releases.
pkgver=1.22.5
pkgrel=0
pkgdesc="GStreamer streaming media framework VA API plug-ins"
url="https://gstreamer.freedesktop.org/"
arch="all"
license="LGPL-2.1-or-later"
makedepends="
	eudev-dev
	glib-dev
	gst-plugins-bad-dev
	libva-dev
	libxrandr-dev
	meson
	"
options="!check" # Need actual display with HW accel
source="https://gstreamer.freedesktop.org/src/gstreamer-vaapi/gstreamer-vaapi-$pkgver.tar.xz"
builddir="$srcdir/gstreamer-vaapi-$pkgver"

# make name more consistent with gst-
provides="gstreamer-vaapi=$pkgver-r$pkgrel"
replaces="gstreamer-vaapi"

build() {
	CFLAGS="$CFLAGS -O2" \
	CXXFLAGS="$CXXFLAGS -O2" \
	CPPFLAGS="$CPPFLAGS -O2" \
	abuild-meson \
		-Db_lto=true \
		-Dtests="$(want_check && echo enabled || echo disabled)" \
		. output
	meson compile -C output
}

check() {
	meson test -C output --print-errorlogs
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
d52c6a6314f35fd8fa3ce009f6d329a30eb94a47876d48e4971a9cb035b8405eebfdfbd0e0cc3947e6b38b6ed63cb9db842fca51d3b1a0a77d7139bec0250b05  gstreamer-vaapi-1.22.5.tar.xz
"
