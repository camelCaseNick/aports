# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kpublictransport
pkgver=23.08.0
pkgrel=0
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://invent.kde.org/libraries/kpublictransport"
pkgdesc="Library to assist with accessing public transport timetables and other data"
license="BSD-3-Clause AND LGPL-2.0-or-later AND MIT"
depends_dev="
	ki18n-dev
	networkmanager-qt-dev
	protobuf-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	zlib-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
_repo_url="https://invent.kde.org/libraries/kpublictransport.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kpublictransport-$pkgver.tar.xz"
options="!check" # Broken for now
subpackages="$pkgname-dev"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7b3537936c056551fad0bf004dc03be2260ba94b4852ecd2ec611a632c61ed22dafe249dd6e4c9a25aa6098b58a705d58a6cf923c9518199597d9468b9cf70b7  kpublictransport-23.08.0.tar.xz
"
