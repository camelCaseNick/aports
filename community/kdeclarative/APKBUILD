# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kdeclarative
pkgver=5.109.0
pkgrel=0
pkgdesc="Provides integration of QML and KDE Frameworks"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	kconfig-dev
	kglobalaccel-dev
	kguiaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kpackage-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	libepoxy-dev
	qt5-qtdeclarative-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/frameworks/kdeclarative.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdeclarative-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# quickviewsharedengine requires OpenGL
	xvfb-run ctest --test-dir build --output-on-failure -E "quickviewsharedengine"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
861bcbba0e228d8c6d5b7618876b38cf0dc9f03d2bc6e51e2cac90c0ae48645f047c1fcccf654f0c51440daac67de3698cd846ccc2ad19cf292a01e2130b5d5d  kdeclarative-5.109.0.tar.xz
"
