# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=yakuake
pkgver=23.08.0
pkgrel=0
arch="all !armhf"
url="https://kde.org/applications/system/org.kde.yakuake"
pkgdesc="A drop-down terminal emulator based on KDE Konsole technology"
license="GPL-2.0-only OR GPL-3.0-only"
depends="konsole"
makedepends="
	extra-cmake-modules
	karchive-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knewstuff-dev
	knotifications-dev
	knotifyconfig-dev
	kparts-dev
	kwayland-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	qt5-qtx11extras-dev
	samurai
	"
_repo_url="https://invent.kde.org/utilities/yakuake.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/yakuake-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
940d60482d6b69c65ff13c746b80786440be156dac135be6fc322112998dc382ca5c7b4526f14dbaa8d2d834f3b0e88d0183e2d40ced83087ccc578e079343e0  yakuake-23.08.0.tar.xz
"
