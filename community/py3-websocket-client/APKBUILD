# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=py3-websocket-client
pkgver=1.6.2
pkgrel=0
pkgdesc="WebSocket client library for Python"
url="https://github.com/websocket-client/websocket-client"
arch="noarch"
license="Apache-2.0"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
checkdepends="
	py3-python-socks
	"
subpackages="$pkgname-pyc"
source="https://github.com/websocket-client/websocket-client/archive/v$pkgver/websocket-client-v$pkgver.tar.gz"
builddir="$srcdir/websocket-client-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m unittest discover
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
	rm -r "$pkgdir"/usr/lib/python3*/site-packages/websocket/tests
}

sha512sums="
274c24e5dbfae647eb80c79066c973f842e474496da1ca032405b1caa98d550fcc3fd9a360692e8135c2a58c278ee81743214be14c5bb42c10eaa657286f35d9  websocket-client-v1.6.2.tar.gz
"
