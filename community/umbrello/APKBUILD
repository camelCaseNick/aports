# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=umbrello
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://umbrello.kde.org/"
pkgdesc="GUI for diagramming Unified Modelling Language (UML)"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	karchive-dev
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdelibs4support-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kitemmodels-dev
	ktexteditor-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/sdk/umbrello.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/umbrello-$pkgver.tar.xz"
options="!check" # Broken

case "$CARCH" in
	ppc64le|armv7) options="!check";; # FIXME: testsuite fails
esac

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_KF5=ON
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
33875df183d4e24eb968dff5d7c91c38f21c31c8cf9bfb8b18710f7d50fa946cdb225bdce58215600e590fa8a465bb6e10cf403853b307929d1584078c0e999c  umbrello-23.08.0.tar.xz
"
