# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kxmlrpcclient
pkgver=5.109.0
pkgrel=0
pkgdesc="XML-RPC client library for KDE"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://projects.kde.org/projects/kde/pim/kxmlrpcclient"
license="BSD-2-Clause"
depends_dev="
	ki18n-dev
	kio-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
_repo_url="https://invent.kde.org/frameworks/kxmlrpcclient.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kxmlrpcclient-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6d5698153f3a177e8b7cf1b2ce6c9a2a2bd9cb1da64b70882a55ba7b624b2bc432834ab50435a1f4efd629f0302bdffbedc1707332aee430d39843d6dd92eb68  kxmlrpcclient-5.109.0.tar.xz
"
