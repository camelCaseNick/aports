# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=kpipewire
pkgver=5.27.7
pkgrel=2
pkgdesc="Components relating to pipewire use in Plasma"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="BSD-3-Clause AND CC0-1.0 AND LGPL-2.1-only AND LGPL-3.0-only AND LicenseRef-KDE-Accepted-LGPL"
depends="pipewire"
depends_dev="
	ffmpeg-dev
	kcoreaddons-dev
	ki18n-dev
	kwayland-dev
	libdrm-dev
	libepoxy-dev
	pipewire-dev
	plasma-wayland-protocols
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtwayland-dev
	wayland-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
subpackages="$pkgname-dev $pkgname-lang"
_repo_url="https://invent.kde.org/plasma/kpipewire.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/kpipewire-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
fc539e29b74c6b9db68b18d9ef87d1cf57c883a20cf5871234a75d21a47de742da43fdf11fa36266ba01263d475fe07e3fe452c71ca329df4945ce463878c380  kpipewire-5.27.7.tar.xz
"
