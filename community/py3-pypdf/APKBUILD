# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=py3-pypdf
pkgver=3.15.5
pkgrel=0
pkgdesc="Pure-Python library built as a PDF toolkit"
url="https://github.com/py-pdf/pypdf"
arch="noarch"
license="BSD-3-Clause"
options="!check" # issues with reading pdf files from test dirs
makedepends="py3-gpep517 py3-installer py3-flit-core"
checkdepends="py3-pillow py3-pytest"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/py-pdf/pypdf/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/pypdf-$pkgver"

provides="py3-pypdf2=$pkgver-r$pkgrel"
replaces="py3-pypdf2"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
173cfbfbbd5f2f08ebd03aa36750904d5a7ac328c27da1b38bda7b34586bf86eab391b3c1e9a6dd5f2e754d1871303afe151a80a3bb5d4ab867b6a019ecef054  py3-pypdf-3.15.5.tar.gz
"
