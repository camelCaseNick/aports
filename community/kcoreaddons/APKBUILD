# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kcoreaddons
pkgver=5.109.0
pkgrel=0
pkgdesc="Addons to QtCore"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev
	breeze-icons
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qttools-dev
	samurai
	shared-mime-info
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/frameworks/kcoreaddons.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcoreaddons-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang kde-default-icon-theme:icons:noarch"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# klistopenfilesjobtest_unix requires >8000 open files
	local disabled_tests="klistopenfilesjobtest_unix|kdirwatch_qfswatch_unittest|kdirwatch_stat_unittest|knetworkmountstestpaths"
	case "$CARCH" in
		s390x) disabled_tests="$disabled_tests|kdirwatch_inotify_unittest" ;;
		x86_64) disabled_tests="$disabled_tests|kfileutilstest" ;; # Only fails on builders
	esac

	xvfb-run ctest --test-dir build --output-on-failure -E "($disabled_tests)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

icons() {
	pkgdesc="Virtual to automatically pull in breeze icons by default for KDE apps"
	install_if="$pkgname=$pkgver-r$pkgrel"
	depends="breeze-icons"

	mkdir -p "$subpkgdir"
}

sha512sums="
d55dc3d7f9edc145b150a49ec0e8f6e975b1c9804940f11a07178dcb1f6d5b45e1b0f4ad2edb51658f639e8bbd26bf7cc3f780db94b270ff0b0febf720f9f513  kcoreaddons-5.109.0.tar.xz
"
