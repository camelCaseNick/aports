# Contributor: Holger Jaekel <holger.jaekel@gmx.de>
# Maintainer: Holger Jaekel <holger.jaekel@gmx.de>
pkgname=google-cloud-cpp
pkgver=2.14.0
_googleapis_commit_sha=85f8c758016c279fb7fa8f0d51ddc7ccc0dd5e05
pkgrel=0
pkgdesc="C++ Client Libraries for Google Cloud Services"
url="https://cloud.google.com/sdk"
arch="all"
license="Apache-2.0"
makedepends="
	abseil-cpp-dev
	c-ares-dev
	cmake
	crc32c-dev
	curl-dev
	grpc-dev
	nlohmann-json
	openssl-dev
	protobuf-dev
	re2-dev
	samurai
	"
checkdepends="benchmark-dev gtest-dev"
subpackages="$pkgname-dev"
source="google-cloud-cpp-$pkgver.tar.gz::https://github.com/googleapis/google-cloud-cpp/archive/refs/tags/v$pkgver.tar.gz
	https://github.com/googleapis/googleapis/archive/$_googleapis_commit_sha.tar.gz
	10-gcc13.patch
	"

_ga_library="
	accessapproval
	accesscontextmanager
	advisorynotifications
	aiplatform
	alloydb
	apigateway
	apigeeconnect
	appengine
	artifactregistry
	asset
	assuredworkloads
	automl
	baremetalsolution
	batch
	beyondcorp
	bigquery
	bigtable
	billing
	binaryauthorization
	certificatemanager
	channel
	cloudbuild
	commerce
	composer
	confidentialcomputing
	connectors
	contactcenterinsights
	containeranalysis
	container
	contentwarehouse
	datacatalog
	datafusion
	datamigration
	dataplex
	dataproc
	datastream
	deploy
	dialogflow_cx
	dialogflow_es
	dlp
	documentai
	domains
	edgecontainer
	essentialcontacts
	eventarc
	filestore
	functions
	gkebackup
	gkehub
	gkemulticloud
	iam
	iap
	ids
	iot
	kms
	language
	logging
	managedidentities
	memcache
	metastore
	migrationcenter
	monitoring
	networkconnectivity
	networkmanagement
	networksecurity
	networkservices
	notebooks
	optimization
	orgpolicy
	osconfig
	oslogin
	policytroubleshooter
	privateca
	profiler
	pubsub
	rapidmigrationassessment
	recaptchaenterprise
	recommender
	redis
	resourcemanager
	resourcesettings
	retail
	run
	scheduler
	secretmanager
	securitycenter
	servicecontrol
	servicedirectory
	servicemanagement
	serviceusage
	shell
	spanner
	speech
	storageinsights
	storagetransfer
	storage
	support
	talent
	tasks
	texttospeech
	timeseriesinsights
	tpu
	trace
	translate
	videointelligence
	video
	vision
	vmmigration
	vmwareengine
	vpcaccess
	webrisk
	websecurityscanner
	workflows
	workstations
	"
_non_ga_library="
	apikeys
	cloud-bigquery-protos
	cloud-common-common-protos
	cloud-dialogflow-v2-protos
	cloud-extended-operations-protos
	cloud-orgpolicy-v1-orgpolicy-protos
	cloud-speech-protos
	cloud-texttospeech-protos
	devtools-cloudtrace-v2-trace-protos
	devtools-cloudtrace-v2-tracing-protos
	devtools-source-v1-source-context-protos
	grafeas-protos
	grpc-utils
	longrunning-operations-protos
	oauth2
	rest-internal
	rest-protobuf-internal
	rpc-code-protos
	rpc-context-attribute-context-protos
	rpc-error-details-protos
	rpc-status-protos
	"
_api_protos="
	api-annotations
	api-auth
	api-backend
	api-billing
	api-client
	api-config-change
	api-consumer
	api-context
	api-control
	api-distribution
	api-documentation
	api-endpoint
	api-error-reason
	api-field-behavior
	api-httpbody
	api-http
	api-label
	api-launch-stage
	api-logging
	api-log
	api-metric
	api-monitored-resource
	api-monitoring
	api-policy
	api-quota
	api-resource
	api-routing
	api-service
	api-source-info
	api-system-parameter
	api-usage
	api-visibility
	"
_type_protos="
	type-calendar-period
	type-color
	type-datetime
	type-date
	type-dayofweek
	type-decimal
	type-expr
	type-fraction
	type-interval
	type-latlng
	type-localized-text
	type-money
	type-month
	type-phone-number
	type-postal-address
	type-quaternion
	type-timeofday
	"

for _lib in $_ga_library $_non_ga_library $_api_protos $_type_protos ; do
	subpackages="$subpackages $pkgname-$_lib:library"
done

prepare() {
	default_prepare

	# google-cloud-cpp needs the proto and gRPC definitions for most
	# Google Cloud services. By default these definitions are downloaded
	# from GitHub during the build process. Using the same SHA is the
	# recommended practice when the googleapis are downloaded outside the
	# build process. In this APKBUILD, we download the googleapis as an
	# additional source. In that case we have to make sure that the SHA
	# match.
	local sha256=$(awk '/_GOOGLE_CLOUD_CPP_GOOGLEAPIS_SHA256/ { getline; print $0 }' cmake/GoogleapisConfig.cmake | tr -d '") ')
	echo "expected sha: $sha256"
	echo "$sha256 *$srcdir/$_googleapis_commit_sha.tar.gz" | sha256sum -c -
}

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	LDFLAGS="$LDFLAGS -Wl,--copy-dt-needed-entries" \
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=None \
		-DBUILD_TESTING="$(want_check && echo ON || echo OFF)" \
		-DGOOGLE_CLOUD_CPP_ENABLE_EXAMPLES=OFF \
		-DGOOGLE_CLOUD_CPP_ENABLE=__ga_libraries__ \
		-DGOOGLE_CLOUD_CPP_OVERRIDE_GOOGLEAPIS_URL=$srcdir/googleapis-$_googleapis_commit_sha \
		$CMAKE_CROSSOPTS
	cmake --build build
}

check() {
	cd build
	timeout 1500 \
		ctest --output-on-failure -LE "integration-test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

library() {
	local name=${subpkgname#"$pkgname"-}
	pkgdesc="$pkgdesc ($name)"
	amove usr/lib/libgoogle_cloud_cpp_${name//-/_}*
}

sha512sums="
c7fd2445339fbb9f66d6863693feff456fa14381e83f3e28456aed2e8102c8d776868afa1a5020874672306f68e911f199b5ce667c61708166925c63877e8c5d  google-cloud-cpp-2.14.0.tar.gz
5d9954c2bb776e10314c9e059552b9fef5f94eee0cf00070049acb35a892976de5aaa8880fe4b7d3cfb22c5abc1d398a90d252edf5a8291023b7ca5c40a71c3d  85f8c758016c279fb7fa8f0d51ddc7ccc0dd5e05.tar.gz
36198eed59eba5388c0206ff1dd6df396ca48f76609eaa8a236ca724f1a116f4f62bb13510e8222f2d2691e9b44b14f2136c4753699f53df5f18ad9af16057ee  10-gcc13.patch
"
