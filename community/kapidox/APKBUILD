# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kapidox
pkgver=5.109.0
pkgrel=0
arch="noarch !armhf" # armhf blocked by extra-cmake-modules
pkgdesc="Scripts and data for building API documentation (dox) in a standard format and style"
url="https://community.kde.org/Frameworks"
license="BSD-3-Clause"
depends="
	doxygen
	py3-jinja2
	py3-yaml
	python3
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	python3-dev
	samurai
	"
checkdepends="bash"
subpackages="$pkgname-pyc"
_repo_url="https://invent.kde.org/frameworks/kapidox.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kapidox-$pkgver.tar.xz"
options="!check" # No useful tests

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
c9bb25a3510c9e3a6ace7155be858718e33fb133bc683b8ab6f23cae1d5ecdd4b73aeb0be97d0a205134ba3d959cfeca1de85b2eb2c2344eae97f6d9b03e7d5e  kapidox-5.109.0.tar.xz
"
