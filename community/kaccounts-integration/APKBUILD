# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kaccounts-integration
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x, ppc64le and riscv64 blocked by signon-ui -> qt5-qtwebengine
arch="all !armhf !s390x !ppc64le !riscv64"
url="https://kde.org/applications/internet/"
pkgdesc="Small system to administer web accounts for the sites and services across the KDE desktop"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends="
	accounts-qml-module
	signon-ui
	"
depends_dev="
	kcmutils-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdeclarative-dev
	ki18n-dev
	libaccounts-qt-dev
	qcoro-dev
	qt5-qtbase-dev
	signond-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
_repo_url="https://invent.kde.org/network/kaccounts-integration.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kaccounts-integration-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="!check" # No tests available

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
73b19dbea2873c951c74f862e88dcfae8394d4b9b7ace609734e171b45045044e2106a433c2670ca83f1aca16e82332c45c5ba938db2e524e28759618c517afb  kaccounts-integration-23.08.0.tar.xz
"
