# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=kmenuedit
pkgver=5.27.7
pkgrel=1
pkgdesc="KDE menu editor"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kdbusaddons-dev
	kdoctools-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kinit-dev
	kio-dev
	kitemviews-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	sonnet-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/kmenuedit.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/kmenuedit-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9a2eb02e6382426d8843f3b4acacfaf95dc124e3ab82dff414e76f83b1f04a9cf81b5428031fa4e742bbb256e38c5ba6e1065a9449f1692e0cbd4786a958bbc3  kmenuedit-5.27.7.tar.xz
"
