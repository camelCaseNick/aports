# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kruler
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/graphics/org.kde.kruler"
pkgdesc="An on-screen ruler for measuring pixels"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kdoctools-dev
	ki18n-dev
	knotifications-dev
	kwindowsystem-dev
	kxmlgui-dev
	qt5-qtbase-dev
	qt5-qtx11extras-dev
	samurai
	"
_repo_url="https://invent.kde.org/graphics/kruler.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kruler-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6be515f419ed3b4a9f0a6b940c6a94681d58cb0f498eda15ddefc5196b29372aa7ca3447d272fd26b0d84bcbb56a9027681ff8ad977486e890cc785b62f14a47  kruler-23.08.0.tar.xz
"
