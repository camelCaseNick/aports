# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kfourinline
pkgver=23.08.0
pkgrel=0
pkgdesc="A four-in-a-row game"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/games/kfourinline/"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdnssd-dev
	kdoctools-dev
	ki18n-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdegames-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	"
_repo_url="https://invent.kde.org/games/kfourinline.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kfourinline-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c5603c1a399bd60ec96dd7e740576b3d62403c14ec70f4c4921720d76e574405db1ac279b16d30028c4481ec3beef0c424ccb38bc4169d4b4957dcb87db863bd  kfourinline-23.08.0.tar.xz
"
