# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=print-manager
pkgver=23.08.0
pkgrel=0
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://www.kde.org/applications/utilities/"
pkgdesc="A tool for managing print jobs and printers"
license="GPL-2.0-or-later"
makedepends="
	cups-dev
	extra-cmake-modules
	kcmutils-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knotifications-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	"
_repo_url="https://invent.kde.org/utilities/print-manager.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/print-manager-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests available

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
0a86bbad3bebb11ebb8cde40f930575d2689edb9d4c70c0749d856a45d50b68babc3765629357c8fd0a66765528d48c6c3e5deee8e06622f755dd2563ffbd705  print-manager-23.08.0.tar.xz
"
