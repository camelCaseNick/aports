# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=cervisia
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/development/org.kde.cervisia"
pkgdesc="A user friendly version control system front-end"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kdesu-dev
	kdoctools-dev
	kiconthemes-dev
	kinit-dev
	kitemviews-dev
	knotifications-dev
	kparts-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/sdk/cervisia.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/cervisia-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
29c718df1ed666235414c952d635a85659427856cfb5517208fcd57ad198f8f6d59c7be0f4c25a7408a17cdab1920ae89d631b7cf03b49633f502b19d47118da  cervisia-23.08.0.tar.xz
"
