# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=ktextwidgets
pkgver=5.109.0
pkgrel=0
pkgdesc="Advanced text editing widgets"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	ki18n-dev
	kiconthemes-dev
	kservice-dev
	kwidgetsaddons-dev
	qt5-qtspeech-dev
	sonnet-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/frameworks/ktextwidgets.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/ktextwidgets-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
5c7c1a3c83995b21c75595f17e31a96ea13067d3c505aada873ef6f22cff9d933365a36232116064fdc4e0b59f349229b28ca35b39e97cad2f532041cda05ee0  ktextwidgets-5.109.0.tar.xz
"
