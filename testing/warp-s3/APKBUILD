# Maintainer: Michał Polański <michal@polanski.me>
pkgname=warp-s3
pkgver=0.7.5
pkgrel=0
pkgdesc="Benchmarking tool for S3"
url="https://github.com/minio/warp"
license="AGPL-3.0"
arch="all !armhf !armv7 !x86" # tests fail on 32-bit architectures
makedepends="go"
source="https://github.com/minio/warp/archive/v$pkgver/warp-s3-$pkgver.tar.gz"
builddir="$srcdir/warp-$pkgver"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build \
		-ldflags "-X github.com/minio/warp/pkg.Version=$pkgver" \
		-o warp-s3
}

check() {
	go test ./...
}

package() {
	install -Dm755 $pkgname -t "$pkgdir"/usr/bin/
}

sha512sums="
8c731da8c7fa450d772b330dded3cbb8564e9b52c67a4283338863f74b7277daefc1fbe097c167f5baed11cb840dbafabb83708fd3309fd4de74cc358f9311b6  warp-s3-0.7.5.tar.gz
"
