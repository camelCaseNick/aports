# Maintainer: Michał Polański <michal@polanski.me>
pkgname=py3-mitmproxy-rs
pkgver=0.2.2
pkgrel=0
pkgdesc="mitmproxy modules written in Rust"
url="https://github.com/mitmproxy/mitmproxy_rs"
license="MIT"
arch="all !ppc64le !riscv64 !s390x" # fails to build ring crate
makedepends="
	cargo
	protoc
	py3-gpep517
	py3-installer
	py3-maturin
	py3-setuptools
	py3-wheel
	"
subpackages="$pkgname-pyc"
source="https://github.com/mitmproxy/mitmproxy_rs/archive/$pkgver/py3-mitmproxy-rs-$pkgver.tar.gz
	dont-use-vendored-protoc.patch
	"
builddir="$srcdir/mitmproxy_rs-$pkgver"
options="net" # cargo

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cd ffi
	gpep517 build-wheel \
		--wheel-dir "$builddir"/.dist \
		--output-fd 3 3>&1 >&2
}

check() {
	cargo test --frozen
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
1d6895c282acb99e5e6159fcbc0da88710b28552d4d124726c01b821c736a2f5ed2e41b15095c27eacecfc0032922aa2fd4966e794eebe26beced938a008b838  py3-mitmproxy-rs-0.2.2.tar.gz
ef7f85337c1444544995b61d15b7b7f63f96b880665f42c14a8879742b137863db934db5f9328a65dcd7f8969590e3c0c53cb01be903a38139c2350af0ec56f0  dont-use-vendored-protoc.patch
"
