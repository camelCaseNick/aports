# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=pdm
pkgver=2.8.2
pkgrel=0
pkgdesc="Modern Python package and dependency manager"
url="https://pdm.fming.dev/"
arch="noarch"
license="MIT"
depends="
	py3-blinker
	py3-certifi
	py3-packaging
	py3-platformdirs
	py3-rich
	py3-virtualenv
	py3-pyproject-hooks
	py3-requests-toolbelt
	py3-unearth
	py3-findpython
	py3-tomlkit
	py3-shellingham
	py3-dotenv
	py3-resolvelib
	py3-installer
	py3-cachecontrol
"
makedepends="py3-pdm-backend py3-gpep517 py3-installer"
checkdepends="py3-pytest py3-pytest-mock py3-pytest-httpserver"
subpackages="$pkgname-pyc"
source="https://github.com/pdm-project/pdm/archive/$pkgver/pdm-$pkgver.tar.gz"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -k 'not test_use_wrapper_python'
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
9fdb5917c7808969d964ee12e19a22b7a54296c3bb44eeaeb8797ec662dd62a1affa2f0edb88f46b6606d770847bef5a1f64d0c8c0f57d6fa65cb98e653f439a  pdm-2.8.2.tar.gz
"
