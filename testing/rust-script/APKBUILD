# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=rust-script
pkgver=0.31.0
pkgrel=0
pkgdesc="Run Rust files and expressions as scripts without any setup or compilation step"
url="https://rust-script.org/"
license="Apache-2.0 OR MIT"
arch="all !armhf" # tests hang
makedepends="cargo cargo-auditable"
source="https://github.com/fornwall/rust-script/archive/$pkgver/rust-script-$pkgver.tar.gz"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/$pkgname -t "$pkgdir"/usr/bin/
}

sha512sums="
f50fc70bba6e33b314a48fa395d35fca629cef6151eac5f047f1adac9099cac3d577aecb8dd636e3b2a5ea2e48fd9aece332cba8d2b4979b0e70c27bf125bd5d  rust-script-0.31.0.tar.gz
"
