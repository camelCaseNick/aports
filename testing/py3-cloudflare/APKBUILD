# Contributor: Lauren N. Liberda <lauren@selfisekai.rocks>
# Maintainer: Lauren N. Liberda <lauren@selfisekai.rocks>
pkgname=py3-cloudflare
pkgver=2.11.7
pkgrel=0
pkgdesc="Python wrapper for the Cloudflare Client API v4"
url="https://github.com/cloudflare/python-cloudflare"
arch="noarch"
license="MIT"
depends="
	py3-beautifulsoup4
	py3-future
	py3-jsonlines
	py3-requests
	py3-yaml
	python3
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
checkdepends="py3-pytest"
subpackages="$pkgname-doc $pkgname-pyc"
source="
	https://github.com/cloudflare/python-cloudflare/archive/refs/tags/$pkgver/python-cloudflare-$pkgver.tar.gz

	no-examples.patch
	"
builddir="$srcdir/python-cloudflare-$pkgver"
options="!check"	# no tests

build() {
	rm -rf examples

	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer \
		-d "$pkgdir" .dist/*.whl
}

sha512sums="
21c474b0465aab19498095e28937aebbc428d5aa70480f7deb5ef1fe74340bfdd7580b66be34eee1f1fc5c2b5f3e2d0666704d3fdda96a69ca139516e60c9a95  python-cloudflare-2.11.7.tar.gz
cc344a2d5724e49dce03fb537d8b0ddc6a6ab80be7ac66d2c2defa4b805368a56074f4ad045b3623530da99aaa2c3b710c6c743636fa4528373b98eb0a35c19b  no-examples.patch
"
