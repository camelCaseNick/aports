# Contributor: Simon Zeni <simon@bl4ckb0ne.ca>
# Maintainer: Simon Zeni <simon@bl4ckb0ne.ca>
pkgname=vulkan-validation-layers
_pkgname=Vulkan-ValidationLayers
pkgver=1.3.261.0
pkgrel=0
pkgdesc="Vulkan Validation Layers"
url="https://www.khronos.org/vulkan/"
arch="all"
license="Apache-2.0"
makedepends="
	cmake
	libx11-dev
	libxcb-dev
	libxrandr-dev
	ninja
	python3
	robin-hood-hashing
	spirv-headers
	spirv-tools-dev
	vulkan-headers
	wayland-dev
	"
subpackages="$pkgname-dbg $pkgname-static $pkgname-dev"
source="$pkgname-$pkgver-2.tar.gz::https://github.com/KhronosGroup/Vulkan-ValidationLayers/archive/refs/tags/sdk-$pkgver.tar.gz
	gcc13.patch
	"
builddir="$srcdir/$_pkgname-sdk-$pkgver"
options="!check" # test segfaults

build() {
	CFLAGS="$CFLAGS -g1" CXXFLAGS="$CXXFLAGS -g1" \
	cmake -B build -G Ninja -Wno-dev \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_INSTALL_DATAROOTDIR=/usr/share \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DBUILD_LAYER_SUPPORT_FILES=ON \
		-DBUILD_WSI_XCB_SUPPORT=ON \
		-DBUILD_WSI_XLIB_SUPPORT=ON \
		-DBUILD_WSI_WAYLAND_SUPPORT=ON \
		-DBUILD_WERROR=OFF \
		-DSPIRV_HEADERS_INSTALL_DIR=/usr

	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
cadd44773701b0937956c1b1f7c6a9f8c574c857af044271a64065367ec8c453dbda04c5bd42e307d7997441e9ad67df5656f18b5a81b315174ec8618cfe0e1a  vulkan-validation-layers-1.3.261.0-2.tar.gz
155406b5e77c68adc85cd430f0630ee649117014e91236acf6026966dfaa2056bd9f6f6cef1c22ddc3e78fe5a35f8e405127002535b1b63bb4aaa84e92fab04d  gcc13.patch
"
