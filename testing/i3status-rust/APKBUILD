# Contributor: Galen Abell <galen@galenabell.com>
# Contributor: Maxim Karasev <begs@disroot.org>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=i3status-rust
pkgver=0.32.1
pkgrel=0
pkgdesc="i3status replacement in Rust"
url="https://github.com/greshake/i3status-rust"
arch="all !s390x !riscv64" # limited by cargo
license="GPL-3.0-only"
makedepends="
	cargo
	cargo-auditable
	curl-dev
	dbus-dev
	lm-sensors-dev
	notmuch-dev
	openssl-dev>3
	pulseaudio-dev
	"
options="net"
provides="i3status-rs=$pkgver-r$pkgrel"
subpackages="$pkgname-doc"
source="
	https://github.com/greshake/i3status-rust/archive/refs/tags/v$pkgver/i3status-rust-v$pkgver.tar.gz
	https://dev.alpinelinux.org/archive/i3status-rs/i3status-rs-$pkgver.1
	"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	cargo test --all-features
}

package() {
	install -Dm755 target/release/i3status-rs -t "$pkgdir"/usr/bin/

	install -Dm644 "$srcdir"/i3status-rs-$pkgver.1 \
		"$pkgdir"/usr/share/man/man1/i3status-rs.1

	install -Dm644 files/themes/* -t "$pkgdir"/usr/share/i3status-rust/themes/
	install -Dm644 files/icons/* -t "$pkgdir"/usr/share/i3status-rust/icons/
}

sha512sums="
eced8ac1525f83e193eb7329618a2858136d45340ea75007ffc454e9ce37750fcb6e3c2afc7e34fb0853c974d8ad66d1c163fd8bab6d7e681ce691a24e695a64  i3status-rust-v0.32.1.tar.gz
92cd369fd7b016250b7e73c139d9d09912264d5a280a908e0d19623f70ad8c72633952d38ea779814240096412d111d196c8c73e3ccf56dc37e75a69e642ee48  i3status-rs-0.32.1.1
"
