# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=py3-findpython
pkgver=0.3.1
pkgrel=0
pkgdesc="Utility to find python versions on your system"
url="https://github.com/frostming/findpython"
arch="noarch"
license="MIT"
depends="py3-packaging"
makedepends="py3-gpep517 py3-installer py3-pdm-backend"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://github.com/frostming/findpython/archive/$pkgver/py3-findpython-$pkgver.tar.gz"
builddir="$srcdir/findpython-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
33d200aabb175124d3df62aff64eee5bd53f071a56ea6f49241908c9db6c28001bf0415b32e766c18ded781f2ed4331c0fadcdc67bc62ea758899ce968be9d89  py3-findpython-0.3.1.tar.gz
"
