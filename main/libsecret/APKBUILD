# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=libsecret
pkgver=0.21.0
pkgrel=0
pkgdesc="Library for storing and retrieving passwords and other secrets"
url="https://wiki.gnome.org/Projects/Libsecret"
arch="all"
license="LGPL-2.1-or-later"
makedepends="glib-dev libxslt-dev docbook-xsl libgcrypt-dev
	gobject-introspection-dev vala meson"
checkdepends="py3-dbus py3-gobject3 xvfb-run dbus-x11"
options="!check" # gjs is in community now
subpackages="$pkgname-static $pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.gnome.org/sources/libsecret/${pkgver%.*}/libsecret-$pkgver.tar.xz"

build() {
	abuild-meson \
		--default-library=both \
		-Dgtk_doc=false \
		output
	meson compile -C output
}

check() {
	xvfb-run meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
666a67f3f06274ab57e1b6e3e855033ce244ab45b9b237851fc06f33ef7922179b5bcbe96a5dc50d1136cbb8d74c2d0af0af4d0ae9de918b4a63b36abb1f2b97  libsecret-0.21.0.tar.xz
"
