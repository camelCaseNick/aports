# Contributor: Bart Ribbers <bribbers@disroot.org>
# Contributor: Simon Zeni <simon@bl4ckb0ne.ca>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=vulkan-headers
_pkgname=Vulkan-Headers
# Please be VERY careful upgrading this - vulkan-headers breaks API even
# on point releases. So please make sure everything using this still builds
# after upgrades
pkgver=1.3.261.0
pkgrel=0
arch="noarch"
url="https://www.vulkan.org/"
pkgdesc="Vulkan header files"
license="Apache-2.0"
makedepends="cmake samurai"
source="https://github.com/khronosgroup/vulkan-headers/archive/refs/tags/sdk-$pkgver/vulkan-headers-v$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-sdk-$pkgver"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7781c2da5a9b7c217bbbd0fe0ce9ee5a4fc218144404a2620063048b672c1cccad2adb3bdc8d49ba92cb0889a62c60d94dc85030995bb0db9bb1bfb2e48e18d3  vulkan-headers-v1.3.261.0.tar.gz
"
